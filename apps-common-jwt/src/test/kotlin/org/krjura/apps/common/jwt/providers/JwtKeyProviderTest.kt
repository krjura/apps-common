package org.krjura.apps.common.jwt.providers

import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.krjura.apps.common.jwt.config.JwtKeyConfiguration
import org.krjura.apps.common.jwt.config.PkiKeyInfo
import org.krjura.apps.common.jwt.ex.JwtException

class JwtKeyProviderTest {

    @Test
    @Throws(JwtException::class)
    fun testKeyParsing() {
        val config = JwtKeyConfigurationTest()
        val provider = JwtKeyProvider(config)

        assertThat(provider.privateKeyId).isEqualTo("1")
        assertThat(provider.privateKey).isNotNull
        assertThat(provider.getPublicKeyById("1")).isNotNull
        assertThat(provider.getPublicKeyById("2")).isNotNull
        assertThat(provider.getPublicKeyById("3")).isNull()
    }

    @Test
    @Throws(JwtException::class)
    fun testKeyParsingWhenInvalidPrivateKey() {
        val config = JwtKeyConfigurationInvalidPrivateKeyTest()
        assertThrows(JwtException::class.java) {
            JwtKeyProvider(config)
        }
    }

    @Test
    @Throws(JwtException::class)
    fun testKeyParsingWhenInvalidPublicKey() {
        val config = JwtKeyConfigurationInvalidPublicKeyTest()
        assertThrows(JwtException::class.java) {
            JwtKeyProvider(config)
        }
    }

    private class JwtKeyConfigurationInvalidPublicKeyTest : JwtKeyConfiguration {
        override fun keyInfos(): List<PkiKeyInfo> {
            return listOf(
                    PkiKeyInfo(
                            "1",
                            "MTIz",
                            "MzQ1"
                    )
            )
        }

        override fun defaultKeyInfo(): PkiKeyInfo {
            return PkiKeyInfo(
                    "1",
                    "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEx7FBOFM+xXaUWV5CG/lc7m6ldcGT5yy0IwwitVF3F3WVkDjpgFlMnNVXptrku+YK5+ceeSEvMarW2dpRmla/8A==",
                    "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgS7D7m72UVCw7tPwAIu9vxl+bvnCPToaLfejk5oF8/LuhRANCAATHsUE4Uz7FdpRZXkIb+VzubqV1wZPnLLQjDCK1UXcXdZWQOOmAWUyc1Vem2uS75grn5x55IS8xqtbZ2lGaVr/w"
            )
        }
    }

    private class JwtKeyConfigurationInvalidPrivateKeyTest : JwtKeyConfiguration {
        override fun keyInfos(): List<PkiKeyInfo> {
            return listOf(
                    PkiKeyInfo(
                            "1",
                            "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEx7FBOFM+xXaUWV5CG/lc7m6ldcGT5yy0IwwitVF3F3WVkDjpgFlMnNVXptrku+YK5+ceeSEvMarW2dpRmla/8A==",
                            "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgS7D7m72UVCw7tPwAIu9vxl+bvnCPToaLfejk5oF8/LuhRANCAATHsUE4Uz7FdpRZXkIb+VzubqV1wZPnLLQjDCK1UXcXdZWQOOmAWUyc1Vem2uS75grn5x55IS8xqtbZ2lGaVr/w"
                    )
            )
        }

        override fun defaultKeyInfo(): PkiKeyInfo {
            return PkiKeyInfo(
                    "1",
                    "MTIz",
                    "MzQ1"
            )
        }
    }

    private class JwtKeyConfigurationTest : JwtKeyConfiguration {
        override fun keyInfos(): List<PkiKeyInfo> {
            return listOf(
                    PkiKeyInfo(
                            "1",
                            "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEx7FBOFM+xXaUWV5CG/lc7m6ldcGT5yy0IwwitVF3F3WVkDjpgFlMnNVXptrku+YK5+ceeSEvMarW2dpRmla/8A==",
                            "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgS7D7m72UVCw7tPwAIu9vxl+bvnCPToaLfejk5oF8/LuhRANCAATHsUE4Uz7FdpRZXkIb+VzubqV1wZPnLLQjDCK1UXcXdZWQOOmAWUyc1Vem2uS75grn5x55IS8xqtbZ2lGaVr/w"
                    ),
                    PkiKeyInfo(
                            "2",
                            "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAERlZSZqVkYxui7FpGCJ26TQntMZ4xFTdk2cOv+f0xaEucx0Da1Z2ZU8mrAjfO320MVEU9XB6eEZwo7lEbBk/bpA==",
                            "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgc/gYKwx3f2nFjNUcJfbj7Y8KKH7mjXTltI/ac/2lVVyhRANCAARGVlJmpWRjG6LsWkYInbpNCe0xnjEVN2TZw6/5/TFoS5zHQNrVnZlTyasCN87fbQxURT1cHp4RnCjuURsGT9uk"
                    )
            )
        }

        override fun defaultKeyInfo(): PkiKeyInfo {
            return PkiKeyInfo(
                    "1",
                    "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEx7FBOFM+xXaUWV5CG/lc7m6ldcGT5yy0IwwitVF3F3WVkDjpgFlMnNVXptrku+YK5+ceeSEvMarW2dpRmla/8A==",
                    "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgS7D7m72UVCw7tPwAIu9vxl+bvnCPToaLfejk5oF8/LuhRANCAATHsUE4Uz7FdpRZXkIb+VzubqV1wZPnLLQjDCK1UXcXdZWQOOmAWUyc1Vem2uS75grn5x55IS8xqtbZ2lGaVr/w"
            )
        }
    }
}