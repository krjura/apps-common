package org.krjura.apps.common.jwt

import com.auth0.jwt.exceptions.InvalidClaimException
import com.auth0.jwt.exceptions.SignatureVerificationException
import com.auth0.jwt.exceptions.TokenExpiredException
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.Test
import org.krjura.apps.common.jwt.config.JwtKeyConfiguration
import org.krjura.apps.common.jwt.config.PkiKeyInfo
import org.krjura.apps.common.jwt.ex.JwtException
import org.krjura.apps.common.jwt.providers.JwtKeyProvider

class JwtTokenGeneratorTest {

    @Test
    @Throws(JwtException::class)
    fun testSessionSuccessfulTokenGeneration() {
        val config = JwtKeyConfigurationTest()
        val provider = JwtKeyProvider(config)
        val generator = JwtTokenGenerator(provider)

        val tokenConfig = TokenConfig(
                issuer = "si",
                audience = "sa",
                expiresAfterMinutes = 60,
                privileges = listOf("ROLE_USER")
        )

        val token = generator.createSessionToken(tokenConfig)

        assertThat(token).isNotNull
        assertThat(token.tokenId).isNotNull
        assertThat(token.expiresAt).isGreaterThan(System.currentTimeMillis())
        assertThat(token.token).isNotNull

        val decodedToken = generator.validateSessionToken(token.token, tokenConfig)

        assertThat(decodedToken).isNotNull
        assertThat(decodedToken.issuer).isEqualTo("si")
        assertThat(decodedToken.audience).containsExactly("sa")
        assertThat(decodedToken.keyId).isEqualTo("1")
        assertThat(decodedToken.getClaim(JwtTokenGenerator.CONST_CLAIM_PRIVILEGES)).isNotNull
        assertThat(decodedToken.getClaim(JwtTokenGenerator.CONST_CLAIM_PRIVILEGES).asList(String::class.java)).containsExactly("ROLE_USER")
    }

    @Test
    @Throws(JwtException::class)
    fun testRefreshSuccessfulTokenGeneration() {
        val config = JwtKeyConfigurationTest()
        val provider = JwtKeyProvider(config)
        val generator = JwtTokenGenerator(provider)

        val tokenConfig = TokenConfig(
                issuer = "ri",
                audience = "ra",
                expiresAfterMinutes = 60,
                privileges = listOf("ROLE_ADMIN")
        )

        val token = generator.createRefreshToken(tokenConfig)

        assertThat(token).isNotNull
        assertThat(token.tokenId).isNotNull
        assertThat(token.expiresAt).isGreaterThan(System.currentTimeMillis())
        assertThat(token.token).isNotNull

        val decodedToken = generator.validateRefreshToken(token.token, tokenConfig)

        assertThat(decodedToken).isNotNull
        assertThat(decodedToken.issuer).isEqualTo("ri")
        assertThat(decodedToken.audience).containsExactly("ra")
        assertThat(decodedToken.keyId).isEqualTo("1")
        assertThat(decodedToken.getClaim(JwtTokenGenerator.CONST_CLAIM_PRIVILEGES)).isNotNull
        assertThat(decodedToken.getClaim(JwtTokenGenerator.CONST_CLAIM_PRIVILEGES).asList(String::class.java)).containsExactly("ROLE_ADMIN")
    }

    @Test
    @Throws(JwtException::class)
    fun testSessionTokenInvalidIssuer() {
        val config = JwtKeyConfigurationTest()
        val provider = JwtKeyProvider(config)
        val generator = JwtTokenGenerator(provider)

        val tokenConfig = TokenConfig(
                issuer = "ri",
                audience = "ra",
                expiresAfterMinutes = 60,
                privileges = listOf("ROLE_ADMIN")
        )

        val tokenInfo = generator.createSessionToken(tokenConfig)

        val decodeTokenConfig = TokenConfig(
                issuer = "invalid",
                audience = "ra",
                expiresAfterMinutes = 60,
                privileges = listOf("ROLE_ADMIN")
        )

        val ex = assertThrows(InvalidClaimException::class.java) {
            generator.validateSessionToken(tokenInfo.token, decodeTokenConfig)
        }

        assertThat(ex.message).isEqualTo("The Claim 'iss' value doesn't match the required issuer.")
    }

    @Test
    @Throws(JwtException::class)
    fun testSessionTokenInvalidAudience() {
        val config = JwtKeyConfigurationTest()
        val provider = JwtKeyProvider(config)
        val generator = JwtTokenGenerator(provider)

        val tokenConfig = TokenConfig(
                issuer = "ri",
                audience = "ra",
                expiresAfterMinutes = 60,
                privileges = listOf("ROLE_ADMIN")
        )

        val tokenInfo = generator.createSessionToken(tokenConfig)

        val decodeTokenConfig = TokenConfig(
                issuer = "ri",
                audience = "invalid",
                expiresAfterMinutes = 60,
                privileges = listOf("ROLE_ADMIN")
        )

        val ex = assertThrows(InvalidClaimException::class.java) {
            generator.validateSessionToken(tokenInfo.token, decodeTokenConfig)
        }

        assertThat(ex.message).isEqualTo("The Claim 'aud' value doesn't contain the required audience.")
    }

    @Test
    @Throws(JwtException::class)
    fun testSessionTokenNoKey() {
        val config = JwtKeyConfigurationTest()
        val provider = JwtKeyProvider(config)
        val generator = JwtTokenGenerator(provider)

        val tokenConfig = TokenConfig(
                issuer = "ri",
                audience = "ra",
                expiresAfterMinutes = 60,
                privileges = listOf("ROLE_ADMIN")
        )

        val tokenInfo = generator.createSessionToken(tokenConfig)

        val decodeConfig = JwtKeyConfigurationNoPublicKeyTest()
        val decodeProvider = JwtKeyProvider(decodeConfig)
        val decodeGenerator = JwtTokenGenerator(decodeProvider)

        val ex = assertThrows(SignatureVerificationException::class.java) {
            decodeGenerator.validateSessionToken(tokenInfo.token, tokenConfig)
        }

        assertThat(ex)
                .hasCauseInstanceOf(IllegalStateException::class.java)
                .hasRootCauseMessage("The given Public Key is null.")
    }

    @Test
    @Throws(JwtException::class)
    fun testSessionTokenExpired() {
        val config = JwtKeyConfigurationTest()
        val provider = JwtKeyProvider(config)
        val generator = JwtTokenGenerator(provider)

        val tokenConfig = TokenConfig(
                issuer = "ri",
                audience = "ra",
                expiresAfterMinutes = -2,
                privileges = listOf("ROLE_ADMIN")
        )

        val tokenInfo = generator.createSessionToken(tokenConfig)

        assertThrows(TokenExpiredException::class.java) {
            generator.validateSessionToken(tokenInfo.token, tokenConfig)
        }
    }

    @Test
    @Throws(JwtException::class)
    fun testInvalidTokenType() {
        val config = JwtKeyConfigurationTest()
        val provider = JwtKeyProvider(config)
        val generator = JwtTokenGenerator(provider)

        val tokenConfig = TokenConfig(
                issuer = "ri",
                audience = "ra",
                expiresAfterMinutes = 60,
                privileges = listOf("ROLE_ADMIN")
        )

        val tokenInfo = generator.createSessionToken(tokenConfig)

        val ex = assertThrows(InvalidClaimException::class.java) {
            generator.validateRefreshToken(tokenInfo.token, tokenConfig)
        }

        assertThat(ex.message).isEqualTo("The Claim 'tokenType' value doesn't match the required one.")
    }

    private class JwtKeyConfigurationTest : JwtKeyConfiguration {
        override fun keyInfos(): List<PkiKeyInfo> {
            return listOf(
                    PkiKeyInfo(
                            "1",
                            "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEx7FBOFM+xXaUWV5CG/lc7m6ldcGT5yy0IwwitVF3F3WVkDjpgFlMnNVXptrku+YK5+ceeSEvMarW2dpRmla/8A==",
                            "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgS7D7m72UVCw7tPwAIu9vxl+bvnCPToaLfejk5oF8/LuhRANCAATHsUE4Uz7FdpRZXkIb+VzubqV1wZPnLLQjDCK1UXcXdZWQOOmAWUyc1Vem2uS75grn5x55IS8xqtbZ2lGaVr/w"
                    ),
                    PkiKeyInfo(
                            "2",
                            "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAERlZSZqVkYxui7FpGCJ26TQntMZ4xFTdk2cOv+f0xaEucx0Da1Z2ZU8mrAjfO320MVEU9XB6eEZwo7lEbBk/bpA==",
                            "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgc/gYKwx3f2nFjNUcJfbj7Y8KKH7mjXTltI/ac/2lVVyhRANCAARGVlJmpWRjG6LsWkYInbpNCe0xnjEVN2TZw6/5/TFoS5zHQNrVnZlTyasCN87fbQxURT1cHp4RnCjuURsGT9uk"
                    )
            )
        }

        override fun defaultKeyInfo(): PkiKeyInfo {
            return PkiKeyInfo(
                    "1",
                    "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEx7FBOFM+xXaUWV5CG/lc7m6ldcGT5yy0IwwitVF3F3WVkDjpgFlMnNVXptrku+YK5+ceeSEvMarW2dpRmla/8A==",
                    "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgS7D7m72UVCw7tPwAIu9vxl+bvnCPToaLfejk5oF8/LuhRANCAATHsUE4Uz7FdpRZXkIb+VzubqV1wZPnLLQjDCK1UXcXdZWQOOmAWUyc1Vem2uS75grn5x55IS8xqtbZ2lGaVr/w"
            )
        }
    }

    private class JwtKeyConfigurationNoPublicKeyTest : JwtKeyConfiguration {
        override fun keyInfos(): List<PkiKeyInfo> {
            return listOf()
        }

        override fun defaultKeyInfo(): PkiKeyInfo {
            return PkiKeyInfo(
                    "1",
                    "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEx7FBOFM+xXaUWV5CG/lc7m6ldcGT5yy0IwwitVF3F3WVkDjpgFlMnNVXptrku+YK5+ceeSEvMarW2dpRmla/8A==",
                    "MIGHAgEAMBMGByqGSM49AgEGCCqGSM49AwEHBG0wawIBAQQgS7D7m72UVCw7tPwAIu9vxl+bvnCPToaLfejk5oF8/LuhRANCAATHsUE4Uz7FdpRZXkIb+VzubqV1wZPnLLQjDCK1UXcXdZWQOOmAWUyc1Vem2uS75grn5x55IS8xqtbZ2lGaVr/w"
            )
        }
    }
}