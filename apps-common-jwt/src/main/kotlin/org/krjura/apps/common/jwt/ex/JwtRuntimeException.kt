package org.krjura.apps.common.jwt.ex

class JwtRuntimeException : RuntimeException {

    constructor(message: String) : super(message)

    constructor(message: String, cause: Throwable) : super(message, cause)
}