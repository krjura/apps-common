package org.krjura.apps.common.jwt

data class TokenInfo(val tokenId: String, val expiresAt: Long, val token: String)