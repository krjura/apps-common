package org.krjura.apps.common.jwt.config

data class PkiKeyInfo(val keyId:String, val publicKey:String, val privateKey: String)