package org.krjura.apps.common.jwt.providers

import java.security.interfaces.ECPrivateKey
import java.security.interfaces.ECPublicKey

data class EcKeyInfo(val keyId: String, val privateKey: ECPrivateKey, val publicKey: ECPublicKey)