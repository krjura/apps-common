package org.krjura.apps.common.jwt.providers

import com.auth0.jwt.interfaces.ECDSAKeyProvider
import org.krjura.apps.common.jwt.config.JwtKeyConfiguration
import org.krjura.apps.common.jwt.config.PkiKeyInfo
import org.krjura.apps.common.jwt.ex.JwtException
import org.krjura.apps.common.jwt.ex.JwtRuntimeException
import java.security.KeyFactory
import java.security.NoSuchAlgorithmException
import java.security.interfaces.ECPrivateKey
import java.security.interfaces.ECPublicKey
import java.security.spec.InvalidKeySpecException
import java.security.spec.PKCS8EncodedKeySpec
import java.security.spec.X509EncodedKeySpec
import java.util.Base64
import java.util.Objects
import java.util.function.Function
import java.util.stream.Collectors

class JwtKeyProvider(config: JwtKeyConfiguration) : ECDSAKeyProvider {

    private val kf: KeyFactory
    private val keyInfos: Map<String, EcKeyInfo>
    private val defaultKeyInfo: EcKeyInfo

    override fun getPublicKeyById(keyId: String): ECPublicKey? {
        return keyInfos[keyId]?.publicKey
    }

    override fun getPrivateKey(): ECPrivateKey {
        return defaultKeyInfo.privateKey
    }

    override fun getPrivateKeyId(): String {
        return defaultKeyInfo.keyId
    }

    @Throws(JwtException::class)
    private fun newKeyFactory(): KeyFactory {
        return try {
            KeyFactory.getInstance("EC")
        } catch (e: NoSuchAlgorithmException) {
            throw JwtException("Cannot create EC key factory", e)
        }
    }

    @Throws(JwtException::class)
    private fun configureDefault(config: JwtKeyConfiguration): EcKeyInfo {
        return try {
            convert(config.defaultKeyInfo())
        } catch (e: JwtRuntimeException) {
            throw JwtException("cannot convert default key info to EC key", e)
        }
    }

    @Throws(JwtException::class)
    private fun configure(config: JwtKeyConfiguration): Map<String, EcKeyInfo> {
        return try {
            config
                    .keyInfos()
                    .stream()
                    .map { pkiKeyInfo: PkiKeyInfo -> convert(pkiKeyInfo) }
                    .collect(Collectors.toMap(EcKeyInfo::keyId, Function.identity()))
        } catch (e: JwtRuntimeException) {
            throw JwtException("cannot map key info to EC keys", e)
        }
    }

    private fun convert(pkiKeyInfo: PkiKeyInfo): EcKeyInfo {

        return try {
            val publicRaw = Base64.getDecoder().decode(pkiKeyInfo.publicKey)
            val publicKeySpec = X509EncodedKeySpec(publicRaw)
            val puk = kf.generatePublic(publicKeySpec) as ECPublicKey
            val privateRaw = Base64.getDecoder().decode(pkiKeyInfo.privateKey)
            val privateKeySpec = PKCS8EncodedKeySpec(privateRaw)
            val prk = kf.generatePrivate(privateKeySpec) as ECPrivateKey

            EcKeyInfo(pkiKeyInfo.keyId, prk, puk)
        } catch (e: InvalidKeySpecException) {
            throw JwtRuntimeException("cannot generate key info for key " + pkiKeyInfo.keyId, e)
        }
    }

    init {
        kf = newKeyFactory()
        keyInfos = configure(config)
        defaultKeyInfo = configureDefault(config)
    }
}