package org.krjura.apps.common.jwt.enums

enum class TokenType {
    SESSION, REFRESH
}