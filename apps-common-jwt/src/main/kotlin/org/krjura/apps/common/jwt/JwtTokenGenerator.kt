package org.krjura.apps.common.jwt

import com.auth0.jwt.JWT
import com.auth0.jwt.algorithms.Algorithm
import com.auth0.jwt.interfaces.DecodedJWT
import org.krjura.apps.common.jwt.enums.TokenType
import org.krjura.apps.common.jwt.providers.JwtKeyProvider
import java.time.ZonedDateTime
import java.util.Date
import java.util.UUID

class JwtTokenGenerator(private val keyProvider: JwtKeyProvider) {

    companion object {
        const val CONST_CLAIM_TOKEN_TYPE = "tokenType"
        const val CONST_CLAIM_PRIVILEGES = "privileges"
    }

    fun createSessionToken(config: TokenConfig): TokenInfo {
        return createToken(config, TokenType.SESSION)
    }

    fun createRefreshToken(config: TokenConfig): TokenInfo {
        return createToken(config, TokenType.REFRESH)
    }

    private fun createToken(config: TokenConfig, type: TokenType): TokenInfo {
        val tokenId = UUID.randomUUID().toString()
        val algorithm = Algorithm.ECDSA256(keyProvider)
        val now = ZonedDateTime.now()
        val expiresAt = now.plusMinutes(config.expiresAfterMinutes)
        val issuedAt = now.minusMinutes(1)

        val token = JWT
                .create()
                .withIssuer(config.issuer)
                .withAudience(config.audience)
                .withExpiresAt(Date.from(expiresAt.toInstant()))
                .withIssuedAt(Date.from(issuedAt.toInstant()))
                .withJWTId(tokenId)
                .withKeyId(keyProvider.privateKeyId)
                .withClaim(CONST_CLAIM_TOKEN_TYPE, type.name)
                .withArrayClaim(CONST_CLAIM_PRIVILEGES, config.privileges.toTypedArray())
                .sign(algorithm)

        return TokenInfo(tokenId, expiresAt.toInstant().toEpochMilli(), token)
    }

    fun validateSessionToken(token: String, config: TokenConfig): DecodedJWT {
        return validateToken(token, config, TokenType.SESSION)
    }

    fun validateRefreshToken(token: String, config: TokenConfig): DecodedJWT {
        return validateToken(token, config, TokenType.REFRESH)
    }

    private fun validateToken(token: String, config: TokenConfig, type: TokenType): DecodedJWT {
        val algorithm = Algorithm.ECDSA256(keyProvider)

        val verifier = JWT.require(algorithm)
                .withIssuer(config.issuer)
                .withAudience(config.audience)
                .withClaim(CONST_CLAIM_TOKEN_TYPE, type.name)
                .acceptLeeway(60)
                .build()

        return verifier.verify(token)
    }
}