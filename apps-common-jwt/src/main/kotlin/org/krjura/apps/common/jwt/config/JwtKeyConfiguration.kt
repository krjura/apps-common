package org.krjura.apps.common.jwt.config

interface JwtKeyConfiguration {

    fun keyInfos(): List<PkiKeyInfo>

    fun defaultKeyInfo(): PkiKeyInfo
}