package org.krjura.apps.common.jwt

data class TokenConfig (
        val issuer: String, val audience: String, val expiresAfterMinutes: Long, val privileges: List<String>
)